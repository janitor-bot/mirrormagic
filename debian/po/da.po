# Danish translation mirrormagic.
# Copyright (C) mirrormagic & nedenstående oversættere.
# This file is distributed under the same license as the mirrormagic package.
# Joe Hansen (joedalton2@yahoo.dk), 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: mirrormagic\n"
"Report-Msgid-Bugs-To: mirrormagic@packages.debian.org\n"
"POT-Creation-Date: 2012-02-04 08:35+0100\n"
"PO-Revision-Date: 2011-05-28 18:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Remove old highscores in /var/lib/games/mirrormagic?"
msgstr "Fjern de gamle bedste resultater i /var/lib/games/mirrormagic?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"You have old highscore files in /var/lib/games/mirrormagic, coming from "
"version 1.3 of mirrormagic. Starting from version 2.0.0, mirrormagic uses a "
"different file format for highscores, and these are placed in /var/games/"
"mirrormagic.  Therefore, /var/lib/games/mirrormagic should be deleted."
msgstr ""
"Du har filer med gamle bedste resultater i /var/lib/games/mirrormagic, der "
"kommer fra version 1.3 af mirrormagic. Fra og med version 2.0.0 bruger "
"mirrormagic et andet format for de bedste resultater, og disse er placeret "
"i /var/games/mirrormagic. Derfør bør /var/lib/games/mirrormagic slettes."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"However, if the old highscores are important to you or if you think you "
"might want to go back to version 1.3, then you may want to keep the old "
"directory."
msgstr ""
"Hvis de gamle bedste resultater er vigtige for dig, eller hvis du tror, du "
"måske ønsker at gå tilbage til version 1.3, kan du beholde den gamle mappe."
